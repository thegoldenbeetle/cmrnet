
import h5py
from pathlib import Path
from tqdm import tqdm
import sys
import asyncio

async def check_h5(f):
        try:
            with h5py.File(f, 'r') as hf:
                #keys = list(hf.keys())#
                pc = hf['PC'][:]
        except Exception as e:
            print(f)

async def main():
    tasks = []
    files = list(Path("/storage/odometry/dataset/sequences/").glob("**/*.h5"))[-1000:]
    bar = tqdm(total=len(files))
    for f in tqdm(files):
        task = check_h5(f)
        tasks.append(task)
        if len(tasks) >= 500:
            await asyncio.gather(*tasks)
            tasks = []
    await asyncio.gather(*tasks)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        loop.close()
    