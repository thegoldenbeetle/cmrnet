INFO - CMRNet - Running command 'main'
INFO - CMRNet - Started run with ID "6"
simple
Test Sequence:  00
TEST SET: Using this file: /storage/odometry/dataset/sequences/test_RT_seq00_2.00_1.00.csv
11423
635
253
Loading weights from ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e18_0.141.tar
11423
Number of model parameters: 37116287
This is 18-th epoch
/home/cmr_user/.local/lib/python3.6/site-packages/torch/optim/lr_scheduler.py:417: UserWarning: To get the last learning rate computed by the scheduler, please use `get_last_lr()`.
  "please use `get_last_lr()`.", UserWarning)
Iter 50/635 training loss = 0.146, time = 0.0162, time for 50 iter: 243.5148
Iter 100/635 training loss = 0.139, time = 0.0155, time for 50 iter: 236.8151
Iter 150/635 training loss = 0.139, time = 0.0161, time for 50 iter: 239.3793
Iter 200/635 training loss = 0.137, time = 0.0159, time for 50 iter: 240.0638
Iter 250/635 training loss = 0.142, time = 0.0162, time for 50 iter: 240.3412
Iter 300/635 training loss = 0.133, time = 0.0155, time for 50 iter: 237.5314
Iter 350/635 training loss = 0.131, time = 0.0159, time for 50 iter: 235.4738
Iter 400/635 training loss = 0.139, time = 0.0157, time for 50 iter: 243.3450
Iter 450/635 training loss = 0.141, time = 0.0155, time for 50 iter: 258.5401
Iter 500/635 training loss = 0.141, time = 0.0153, time for 50 iter: 314.6347
Iter 550/635 training loss = 0.143, time = 0.0159, time for 50 iter: 283.2309
Iter 600/635 training loss = 0.137, time = 0.0159, time for 50 iter: 329.4464
------------------------------------
epoch 18 total training loss = 0.139
Total epoch time = 3261.60
------------------------------------
Iter 50 test loss = 0.121 , time = 0.01
Iter 100 test loss = 0.112 , time = 0.01
Iter 150 test loss = 0.137 , time = 0.01
Iter 200 test loss = 0.135 , time = 0.01
Iter 250 test loss = 0.154 , time = 0.01
------------------------------------
total test loss = 0.131
total traslation error: 40.487967183059126 cm
total rotation error: 1.2570614189966165
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e18_0.131.tar
This is 19-th epoch
Iter 50/635 training loss = 0.131, time = 0.0157, time for 50 iter: 279.6117
Iter 100/635 training loss = 0.130, time = 0.0158, time for 50 iter: 289.5820
Iter 150/635 training loss = 0.133, time = 0.0161, time for 50 iter: 246.8793
Iter 200/635 training loss = 0.140, time = 0.0161, time for 50 iter: 302.0667
Iter 250/635 training loss = 0.143, time = 0.0163, time for 50 iter: 340.8402
Iter 300/635 training loss = 0.132, time = 0.0158, time for 50 iter: 289.8503
Iter 350/635 training loss = 0.142, time = 0.0158, time for 50 iter: 263.9471
Iter 400/635 training loss = 0.132, time = 0.0161, time for 50 iter: 325.6914
Iter 450/635 training loss = 0.133, time = 0.0152, time for 50 iter: 284.0068
Iter 500/635 training loss = 0.138, time = 0.0157, time for 50 iter: 285.3873
Iter 550/635 training loss = 0.134, time = 0.0161, time for 50 iter: 389.2170
Iter 600/635 training loss = 0.135, time = 0.0158, time for 50 iter: 252.7650
------------------------------------
epoch 19 total training loss = 0.135
Total epoch time = 3741.91
------------------------------------
Iter 50 test loss = 0.137 , time = 0.01
Iter 100 test loss = 0.119 , time = 0.01
Iter 150 test loss = 0.140 , time = 0.01
Iter 200 test loss = 0.137 , time = 0.01
Iter 250 test loss = 0.162 , time = 0.01
------------------------------------
total test loss = 0.138
total traslation error: 41.77383818686258 cm
total rotation error: 1.3467870027424087
------------------------------------
This is 20-th epoch
Iter 50/635 training loss = 0.137, time = 0.0157, time for 50 iter: 316.5920
Iter 100/635 training loss = 0.125, time = 0.0155, time for 50 iter: 279.0353
Iter 150/635 training loss = 0.138, time = 0.0155, time for 50 iter: 295.7891
Iter 200/635 training loss = 0.126, time = 0.0158, time for 50 iter: 371.6051
Iter 250/635 training loss = 0.130, time = 0.0154, time for 50 iter: 237.9884
Iter 300/635 training loss = 0.133, time = 0.0159, time for 50 iter: 406.5300
Iter 350/635 training loss = 0.130, time = 0.0153, time for 50 iter: 281.1977
Iter 400/635 training loss = 0.136, time = 0.0159, time for 50 iter: 303.1239
Iter 450/635 training loss = 0.124, time = 0.0164, time for 50 iter: 426.7470
Iter 500/635 training loss = 0.134, time = 0.0151, time for 50 iter: 283.3852
Iter 550/635 training loss = 0.134, time = 0.0160, time for 50 iter: 310.1394
Iter 600/635 training loss = 0.134, time = 0.0159, time for 50 iter: 404.8253
------------------------------------
epoch 20 total training loss = 0.131
Total epoch time = 4138.20
------------------------------------
Iter 50 test loss = 0.136 , time = 0.01
Iter 100 test loss = 0.111 , time = 0.01
Iter 150 test loss = 0.140 , time = 0.01
Iter 200 test loss = 0.134 , time = 0.01
Iter 250 test loss = 0.164 , time = 0.01
------------------------------------
total test loss = 0.136
total traslation error: 42.00497742304059 cm
total rotation error: 1.208514869121223
------------------------------------
This is 21-th epoch
Iter 50/635 training loss = 0.131, time = 0.0155, time for 50 iter: 300.9989
Iter 100/635 training loss = 0.129, time = 0.0153, time for 50 iter: 274.9468
Iter 150/635 training loss = 0.123, time = 0.0157, time for 50 iter: 361.9860
Iter 200/635 training loss = 0.131, time = 0.0156, time for 50 iter: 310.2903
Iter 250/635 training loss = 0.135, time = 0.0152, time for 50 iter: 307.9124
Iter 300/635 training loss = 0.142, time = 0.0156, time for 50 iter: 354.8742
Iter 350/635 training loss = 0.124, time = 0.0155, time for 50 iter: 305.8421
Iter 400/635 training loss = 0.126, time = 0.0155, time for 50 iter: 299.1608
Iter 450/635 training loss = 0.126, time = 0.0157, time for 50 iter: 314.8862
Iter 500/635 training loss = 0.124, time = 0.0153, time for 50 iter: 381.0468
Iter 550/635 training loss = 0.128, time = 0.0158, time for 50 iter: 300.7051
Iter 600/635 training loss = 0.125, time = 0.0156, time for 50 iter: 436.0661
------------------------------------
epoch 21 total training loss = 0.128
Total epoch time = 4111.43
------------------------------------
Iter 50 test loss = 0.122 , time = 0.01
Iter 100 test loss = 0.110 , time = 0.01
Iter 150 test loss = 0.126 , time = 0.01
Iter 200 test loss = 0.123 , time = 0.01
Iter 250 test loss = 0.147 , time = 0.01
------------------------------------
total test loss = 0.125
total traslation error: 39.5127452689891 cm
total rotation error: 1.2712222300707783
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e21_0.125.tar
This is 22-th epoch
Iter 50/635 training loss = 0.130, time = 0.0159, time for 50 iter: 344.3248
Iter 100/635 training loss = 0.128, time = 0.0159, time for 50 iter: 239.9598
Iter 150/635 training loss = 0.124, time = 0.0156, time for 50 iter: 379.4136
Iter 200/635 training loss = 0.134, time = 0.0154, time for 50 iter: 342.5954
Iter 250/635 training loss = 0.123, time = 0.0158, time for 50 iter: 260.3871
Iter 300/635 training loss = 0.130, time = 0.0163, time for 50 iter: 406.5688
Iter 350/635 training loss = 0.126, time = 0.0159, time for 50 iter: 290.0516
Iter 400/635 training loss = 0.126, time = 0.0156, time for 50 iter: 317.0254
Iter 450/635 training loss = 0.125, time = 0.0157, time for 50 iter: 424.1119
Iter 500/635 training loss = 0.127, time = 0.0159, time for 50 iter: 269.3183
Iter 550/635 training loss = 0.120, time = 0.0160, time for 50 iter: 353.8047
Iter 600/635 training loss = 0.122, time = 0.0155, time for 50 iter: 377.1430
------------------------------------
epoch 22 total training loss = 0.126
Total epoch time = 4171.29
------------------------------------
Iter 50 test loss = 0.129 , time = 0.01
Iter 100 test loss = 0.115 , time = 0.01
Iter 150 test loss = 0.125 , time = 0.01
Iter 200 test loss = 0.128 , time = 0.01
Iter 250 test loss = 0.161 , time = 0.01
------------------------------------
total test loss = 0.131
total traslation error: 41.35734093386128 cm
total rotation error: 1.2103863639260304
------------------------------------
This is 23-th epoch
Iter 50/635 training loss = 0.130, time = 0.0160, time for 50 iter: 318.2803
Iter 100/635 training loss = 0.118, time = 0.0157, time for 50 iter: 360.4305
Iter 150/635 training loss = 0.120, time = 0.0162, time for 50 iter: 355.2375
Iter 200/635 training loss = 0.111, time = 0.0153, time for 50 iter: 255.2564
Iter 250/635 training loss = 0.124, time = 0.0158, time for 50 iter: 329.1321
Iter 300/635 training loss = 0.125, time = 0.0160, time for 50 iter: 366.2326
Iter 350/635 training loss = 0.122, time = 0.0160, time for 50 iter: 242.9115
Iter 400/635 training loss = 0.120, time = 0.0155, time for 50 iter: 341.7976
Iter 450/635 training loss = 0.120, time = 0.0155, time for 50 iter: 424.1928
Iter 500/635 training loss = 0.121, time = 0.0154, time for 50 iter: 299.5936
Iter 550/635 training loss = 0.116, time = 0.0154, time for 50 iter: 311.9088
Iter 600/635 training loss = 0.123, time = 0.0156, time for 50 iter: 309.4172
------------------------------------
epoch 23 total training loss = 0.121
Total epoch time = 4201.69
------------------------------------
Iter 50 test loss = 0.116 , time = 0.01
Iter 100 test loss = 0.099 , time = 0.01
Iter 150 test loss = 0.129 , time = 0.01
Iter 200 test loss = 0.122 , time = 0.01
Iter 250 test loss = 0.137 , time = 0.01
------------------------------------
total test loss = 0.120
total traslation error: 38.67182805763418 cm
total rotation error: 1.2200540664620767
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e23_0.120.tar
This is 24-th epoch
Iter 50/635 training loss = 0.117, time = 0.0158, time for 50 iter: 328.5577
Iter 100/635 training loss = 0.122, time = 0.0157, time for 50 iter: 342.9533
Iter 150/635 training loss = 0.120, time = 0.0156, time for 50 iter: 237.7354
Iter 200/635 training loss = 0.123, time = 0.0159, time for 50 iter: 282.8603
Iter 250/635 training loss = 0.118, time = 0.0160, time for 50 iter: 402.9421
Iter 300/635 training loss = 0.110, time = 0.0157, time for 50 iter: 342.5131
Iter 350/635 training loss = 0.120, time = 0.0157, time for 50 iter: 237.8067
Iter 400/635 training loss = 0.115, time = 0.0154, time for 50 iter: 339.7117
Iter 450/635 training loss = 0.131, time = 0.0157, time for 50 iter: 422.9942
Iter 500/635 training loss = 0.108, time = 0.0161, time for 50 iter: 342.4431
Iter 550/635 training loss = 0.113, time = 0.0154, time for 50 iter: 262.6601
Iter 600/635 training loss = 0.121, time = 0.0155, time for 50 iter: 423.8592
------------------------------------
epoch 24 total training loss = 0.117
Total epoch time = 4197.69
------------------------------------
Iter 50 test loss = 0.112 , time = 0.01
Iter 100 test loss = 0.106 , time = 0.01
Iter 150 test loss = 0.129 , time = 0.01
Iter 200 test loss = 0.124 , time = 0.01
Iter 250 test loss = 0.144 , time = 0.01
------------------------------------
total test loss = 0.123
total traslation error: 39.16716777278158 cm
total rotation error: 1.2094227666504134
------------------------------------
This is 25-th epoch
Iter 50/635 training loss = 0.120, time = 0.0156, time for 50 iter: 285.7591
Iter 100/635 training loss = 0.115, time = 0.0158, time for 50 iter: 346.4145
Iter 150/635 training loss = 0.101, time = 0.0158, time for 50 iter: 273.8446
Iter 200/635 training loss = 0.118, time = 0.0156, time for 50 iter: 352.9902
Iter 250/635 training loss = 0.123, time = 0.0156, time for 50 iter: 283.0329
Iter 300/635 training loss = 0.114, time = 0.0157, time for 50 iter: 291.5698
Iter 350/635 training loss = 0.116, time = 0.0160, time for 50 iter: 388.0641
Iter 400/635 training loss = 0.109, time = 0.0155, time for 50 iter: 344.3463
Iter 450/635 training loss = 0.110, time = 0.0155, time for 50 iter: 259.6811
Iter 500/635 training loss = 0.114, time = 0.0161, time for 50 iter: 271.5542
Iter 550/635 training loss = 0.107, time = 0.0155, time for 50 iter: 386.8620
Iter 600/635 training loss = 0.119, time = 0.0160, time for 50 iter: 356.7740
------------------------------------
epoch 25 total training loss = 0.113
Total epoch time = 4003.69
------------------------------------
Iter 50 test loss = 0.118 , time = 0.01
Iter 100 test loss = 0.107 , time = 0.01
Iter 150 test loss = 0.135 , time = 0.01
Iter 200 test loss = 0.121 , time = 0.01
Iter 250 test loss = 0.137 , time = 0.01
------------------------------------
total test loss = 0.123
total traslation error: 39.215420862200816 cm
total rotation error: 1.2137730416157413
------------------------------------
This is 26-th epoch
Iter 50/635 training loss = 0.120, time = 0.0157, time for 50 iter: 284.0546
Iter 100/635 training loss = 0.116, time = 0.0160, time for 50 iter: 330.1036
Iter 150/635 training loss = 0.122, time = 0.0155, time for 50 iter: 353.7166
Iter 200/635 training loss = 0.107, time = 0.0157, time for 50 iter: 295.5708
Iter 250/635 training loss = 0.113, time = 0.0156, time for 50 iter: 246.2182
Iter 300/635 training loss = 0.119, time = 0.0165, time for 50 iter: 364.3134
Iter 350/635 training loss = 0.107, time = 0.0162, time for 50 iter: 387.8878
Iter 400/635 training loss = 0.117, time = 0.0157, time for 50 iter: 348.9026
Iter 450/635 training loss = 0.112, time = 0.0160, time for 50 iter: 239.9644
Iter 500/635 training loss = 0.114, time = 0.0161, time for 50 iter: 375.2146
Iter 550/635 training loss = 0.123, time = 0.0164, time for 50 iter: 431.3583
Iter 600/635 training loss = 0.115, time = 0.0153, time for 50 iter: 254.9578
------------------------------------
epoch 26 total training loss = 0.115
Total epoch time = 4074.85
------------------------------------
Iter 50 test loss = 0.117 , time = 0.01
Iter 100 test loss = 0.100 , time = 0.01
Iter 150 test loss = 0.125 , time = 0.01
Iter 200 test loss = 0.118 , time = 0.01
Iter 250 test loss = 0.134 , time = 0.01
------------------------------------
total test loss = 0.118
total traslation error: 38.128154910049155 cm
total rotation error: 1.2528493778738674
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e26_0.118.tar
This is 27-th epoch
Iter 50/635 training loss = 0.114, time = 0.0156, time for 50 iter: 307.4690
Iter 100/635 training loss = 0.104, time = 0.0161, time for 50 iter: 307.0099
Iter 150/635 training loss = 0.110, time = 0.0160, time for 50 iter: 363.4481
Iter 200/635 training loss = 0.113, time = 0.0162, time for 50 iter: 281.4897
Iter 250/635 training loss = 0.116, time = 0.0159, time for 50 iter: 275.5446
Iter 300/635 training loss = 0.110, time = 0.0153, time for 50 iter: 380.7404
Iter 350/635 training loss = 0.109, time = 0.0158, time for 50 iter: 300.5124
Iter 400/635 training loss = 0.109, time = 0.0158, time for 50 iter: 248.3489
Iter 450/635 training loss = 0.112, time = 0.0156, time for 50 iter: 315.2128
Iter 500/635 training loss = 0.108, time = 0.0165, time for 50 iter: 403.0013
Iter 550/635 training loss = 0.106, time = 0.0158, time for 50 iter: 344.9379
Iter 600/635 training loss = 0.113, time = 0.0154, time for 50 iter: 388.6012
------------------------------------
epoch 27 total training loss = 0.110
Total epoch time = 4105.17
------------------------------------
Iter 50 test loss = 0.100 , time = 0.01
Iter 100 test loss = 0.101 , time = 0.01
Iter 150 test loss = 0.119 , time = 0.01
Iter 200 test loss = 0.111 , time = 0.01
Iter 250 test loss = 0.127 , time = 0.01
------------------------------------
total test loss = 0.111
total traslation error: 36.86115061430856 cm
total rotation error: 1.1650746818471915
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e27_0.111.tar
This is 28-th epoch
Iter 50/635 training loss = 0.108, time = 0.0157, time for 50 iter: 311.3136
Iter 100/635 training loss = 0.114, time = 0.0160, time for 50 iter: 370.8962
Iter 150/635 training loss = 0.112, time = 0.0158, time for 50 iter: 294.8108
Iter 200/635 training loss = 0.105, time = 0.0153, time for 50 iter: 249.4905
Iter 250/635 training loss = 0.105, time = 0.0154, time for 50 iter: 381.8099
Iter 300/635 training loss = 0.110, time = 0.0157, time for 50 iter: 383.9924
Iter 350/635 training loss = 0.105, time = 0.0153, time for 50 iter: 248.4238
Iter 400/635 training loss = 0.107, time = 0.0158, time for 50 iter: 351.9830
Iter 450/635 training loss = 0.103, time = 0.0159, time for 50 iter: 409.4466
Iter 500/635 training loss = 0.103, time = 0.0154, time for 50 iter: 266.7313
Iter 550/635 training loss = 0.107, time = 0.0159, time for 50 iter: 318.0060
Iter 600/635 training loss = 0.103, time = 0.0159, time for 50 iter: 304.6313
------------------------------------
epoch 28 total training loss = 0.107
Total epoch time = 4173.51
------------------------------------
Iter 50 test loss = 0.107 , time = 0.01
Iter 100 test loss = 0.090 , time = 0.01
Iter 150 test loss = 0.114 , time = 0.01
Iter 200 test loss = 0.106 , time = 0.01
Iter 250 test loss = 0.127 , time = 0.01
------------------------------------
total test loss = 0.109
total traslation error: 36.26013459804378 cm
total rotation error: 1.168768069996652
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e28_0.109.tar
This is 29-th epoch
Iter 50/635 training loss = 0.107, time = 0.0155, time for 50 iter: 326.2687
Iter 100/635 training loss = 0.106, time = 0.0150, time for 50 iter: 342.0953
Iter 150/635 training loss = 0.102, time = 0.0161, time for 50 iter: 362.6999
Iter 200/635 training loss = 0.101, time = 0.0155, time for 50 iter: 233.7367
Iter 250/635 training loss = 0.101, time = 0.0158, time for 50 iter: 299.0859
Iter 300/635 training loss = 0.115, time = 0.0157, time for 50 iter: 376.2953
Iter 350/635 training loss = 0.101, time = 0.0159, time for 50 iter: 363.3597
Iter 400/635 training loss = 0.108, time = 0.0156, time for 50 iter: 242.4883
Iter 450/635 training loss = 0.108, time = 0.0163, time for 50 iter: 343.9441
Iter 500/635 training loss = 0.099, time = 0.0156, time for 50 iter: 412.7805
Iter 550/635 training loss = 0.103, time = 0.0156, time for 50 iter: 281.3366
Iter 600/635 training loss = 0.100, time = 0.0158, time for 50 iter: 285.8799
------------------------------------
epoch 29 total training loss = 0.105
Total epoch time = 4091.50
------------------------------------
Iter 50 test loss = 0.105 , time = 0.01
Iter 100 test loss = 0.100 , time = 0.01
Iter 150 test loss = 0.116 , time = 0.01
Iter 200 test loss = 0.115 , time = 0.01
Iter 250 test loss = 0.131 , time = 0.01
------------------------------------
total test loss = 0.113
total traslation error: 37.40931699980043 cm
total rotation error: 1.1706537596587698
------------------------------------
This is 30-th epoch
Iter 50/635 training loss = 0.110, time = 0.0157, time for 50 iter: 300.5008
Iter 100/635 training loss = 0.101, time = 0.0158, time for 50 iter: 295.2788
Iter 150/635 training loss = 0.102, time = 0.0155, time for 50 iter: 242.7268
Iter 200/635 training loss = 0.100, time = 0.0158, time for 50 iter: 358.9945
Iter 250/635 training loss = 0.113, time = 0.0156, time for 50 iter: 379.8899
Iter 300/635 training loss = 0.105, time = 0.0155, time for 50 iter: 341.7833
Iter 350/635 training loss = 0.103, time = 0.0156, time for 50 iter: 245.4636
Iter 400/635 training loss = 0.109, time = 0.0155, time for 50 iter: 350.9869
Iter 450/635 training loss = 0.106, time = 0.0152, time for 50 iter: 324.8995
Iter 500/635 training loss = 0.099, time = 0.0157, time for 50 iter: 419.7598
Iter 550/635 training loss = 0.099, time = 0.0157, time for 50 iter: 318.6725
Iter 600/635 training loss = 0.108, time = 0.0154, time for 50 iter: 393.4101
------------------------------------
epoch 30 total training loss = 0.104
Total epoch time = 4136.88
------------------------------------
Iter 50 test loss = 0.104 , time = 0.01
Iter 100 test loss = 0.092 , time = 0.01
Iter 150 test loss = 0.105 , time = 0.01
Iter 200 test loss = 0.105 , time = 0.01
Iter 250 test loss = 0.135 , time = 0.01
------------------------------------
total test loss = 0.108
total traslation error: 35.85458286993568 cm
total rotation error: 1.158948141686902
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e30_0.108.tar
This is 31-th epoch
Iter 50/635 training loss = 0.100, time = 0.0159, time for 50 iter: 320.5452
Iter 100/635 training loss = 0.110, time = 0.0162, time for 50 iter: 361.3236
Iter 150/635 training loss = 0.100, time = 0.0158, time for 50 iter: 244.7235
Iter 200/635 training loss = 0.107, time = 0.0162, time for 50 iter: 322.1491
Iter 250/635 training loss = 0.105, time = 0.0153, time for 50 iter: 381.8632
Iter 300/635 training loss = 0.101, time = 0.0160, time for 50 iter: 330.7604
Iter 350/635 training loss = 0.101, time = 0.0163, time for 50 iter: 348.5534
Iter 400/635 training loss = 0.100, time = 0.0158, time for 50 iter: 384.9235
Iter 450/635 training loss = 0.103, time = 0.0159, time for 50 iter: 245.3782
Iter 500/635 training loss = 0.098, time = 0.0156, time for 50 iter: 344.9498
Iter 550/635 training loss = 0.105, time = 0.0155, time for 50 iter: 411.9410
Iter 600/635 training loss = 0.099, time = 0.0152, time for 50 iter: 248.8881
------------------------------------
epoch 31 total training loss = 0.102
Total epoch time = 4146.64
------------------------------------
Iter 50 test loss = 0.106 , time = 0.01
Iter 100 test loss = 0.090 , time = 0.01
Iter 150 test loss = 0.112 , time = 0.01
Iter 200 test loss = 0.104 , time = 0.01
Iter 250 test loss = 0.131 , time = 0.01
------------------------------------
total test loss = 0.108
total traslation error: 36.01694403679669 cm
total rotation error: 1.12750558846849
------------------------------------
This is 32-th epoch
Iter 50/635 training loss = 0.104, time = 0.0161, time for 50 iter: 330.3005
Iter 100/635 training loss = 0.096, time = 0.0159, time for 50 iter: 295.5836
Iter 150/635 training loss = 0.098, time = 0.0159, time for 50 iter: 350.3553
Iter 200/635 training loss = 0.106, time = 0.0156, time for 50 iter: 362.7055
Iter 250/635 training loss = 0.100, time = 0.0154, time for 50 iter: 343.1262
Iter 300/635 training loss = 0.107, time = 0.0153, time for 50 iter: 239.6791
Iter 350/635 training loss = 0.093, time = 0.0162, time for 50 iter: 358.8197
Iter 400/635 training loss = 0.097, time = 0.0153, time for 50 iter: 397.2541
Iter 450/635 training loss = 0.093, time = 0.0155, time for 50 iter: 260.8159
Iter 500/635 training loss = 0.094, time = 0.0160, time for 50 iter: 304.2104
Iter 550/635 training loss = 0.101, time = 0.0158, time for 50 iter: 424.0939
Iter 600/635 training loss = 0.098, time = 0.0156, time for 50 iter: 317.9846
------------------------------------
epoch 32 total training loss = 0.098
Total epoch time = 4151.58
------------------------------------
Iter 50 test loss = 0.100 , time = 0.01
Iter 100 test loss = 0.095 , time = 0.01
Iter 150 test loss = 0.114 , time = 0.01
Iter 200 test loss = 0.102 , time = 0.01
Iter 250 test loss = 0.129 , time = 0.01
------------------------------------
total test loss = 0.108
total traslation error: 36.16370295239188 cm
total rotation error: 1.1231583769272613
------------------------------------
This is 33-th epoch
Iter 50/635 training loss = 0.103, time = 0.0157, time for 50 iter: 259.8436
Iter 100/635 training loss = 0.094, time = 0.0156, time for 50 iter: 365.1904
Iter 150/635 training loss = 0.097, time = 0.0159, time for 50 iter: 366.6067
Iter 200/635 training loss = 0.099, time = 0.0160, time for 50 iter: 355.9184
Iter 250/635 training loss = 0.105, time = 0.0154, time for 50 iter: 247.9446
Iter 300/635 training loss = 0.104, time = 0.0159, time for 50 iter: 327.3474
Iter 350/635 training loss = 0.100, time = 0.0163, time for 50 iter: 242.9942
Iter 400/635 training loss = 0.096, time = 0.0156, time for 50 iter: 357.0169
Iter 450/635 training loss = 0.102, time = 0.0161, time for 50 iter: 396.3691
Iter 500/635 training loss = 0.098, time = 0.0159, time for 50 iter: 402.6213
Iter 550/635 training loss = 0.101, time = 0.0157, time for 50 iter: 247.3892
Iter 600/635 training loss = 0.102, time = 0.0166, time for 50 iter: 325.4853
------------------------------------
epoch 33 total training loss = 0.100
Total epoch time = 4175.74
------------------------------------
Iter 50 test loss = 0.106 , time = 0.01
Iter 100 test loss = 0.099 , time = 0.01
Iter 150 test loss = 0.116 , time = 0.01
Iter 200 test loss = 0.109 , time = 0.01
Iter 250 test loss = 0.123 , time = 0.01
------------------------------------
total test loss = 0.110
total traslation error: 36.855470483876935 cm
total rotation error: 1.1663523925818213
------------------------------------
This is 34-th epoch
Iter 50/635 training loss = 0.102, time = 0.0153, time for 50 iter: 361.1569
Iter 100/635 training loss = 0.102, time = 0.0155, time for 50 iter: 363.3878
Iter 150/635 training loss = 0.103, time = 0.0157, time for 50 iter: 287.4480
Iter 200/635 training loss = 0.098, time = 0.0161, time for 50 iter: 364.5231
Iter 250/635 training loss = 0.101, time = 0.0158, time for 50 iter: 270.1861
Iter 300/635 training loss = 0.094, time = 0.0157, time for 50 iter: 264.3807
Iter 350/635 training loss = 0.097, time = 0.0155, time for 50 iter: 416.6076
Iter 400/635 training loss = 0.095, time = 0.0158, time for 50 iter: 393.7441
Iter 450/635 training loss = 0.097, time = 0.0159, time for 50 iter: 302.3674
Iter 500/635 training loss = 0.096, time = 0.0154, time for 50 iter: 305.8148
Iter 550/635 training loss = 0.097, time = 0.0159, time for 50 iter: 429.9861
Iter 600/635 training loss = 0.091, time = 0.0153, time for 50 iter: 413.9214
------------------------------------
epoch 34 total training loss = 0.098
Total epoch time = 4344.07
------------------------------------
Iter 50 test loss = 0.096 , time = 0.01
Iter 100 test loss = 0.091 , time = 0.01
Iter 150 test loss = 0.106 , time = 0.01
Iter 200 test loss = 0.103 , time = 0.01
Iter 250 test loss = 0.133 , time = 0.01
------------------------------------
total test loss = 0.105
total traslation error: 35.83992625700432 cm
total rotation error: 1.1662482524385434
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e34_0.105.tar
This is 35-th epoch
Iter 50/635 training loss = 0.100, time = 0.0158, time for 50 iter: 255.4089
Iter 100/635 training loss = 0.092, time = 0.0161, time for 50 iter: 345.7396
Iter 150/635 training loss = 0.094, time = 0.0158, time for 50 iter: 298.7478
Iter 200/635 training loss = 0.092, time = 0.0151, time for 50 iter: 359.7589
Iter 250/635 training loss = 0.090, time = 0.0159, time for 50 iter: 375.0881
Iter 300/635 training loss = 0.096, time = 0.0156, time for 50 iter: 307.5833
Iter 350/635 training loss = 0.092, time = 0.0158, time for 50 iter: 317.5611
Iter 400/635 training loss = 0.099, time = 0.0157, time for 50 iter: 373.1167
Iter 450/635 training loss = 0.094, time = 0.0160, time for 50 iter: 271.2986
Iter 500/635 training loss = 0.092, time = 0.0157, time for 50 iter: 412.9098
Iter 550/635 training loss = 0.090, time = 0.0157, time for 50 iter: 270.4127
Iter 600/635 training loss = 0.088, time = 0.0154, time for 50 iter: 351.8841
------------------------------------
epoch 35 total training loss = 0.093
Total epoch time = 4218.55
------------------------------------
Iter 50 test loss = 0.097 , time = 0.01
Iter 100 test loss = 0.091 , time = 0.01
Iter 150 test loss = 0.104 , time = 0.01
Iter 200 test loss = 0.097 , time = 0.01
Iter 250 test loss = 0.124 , time = 0.01
------------------------------------
total test loss = 0.102
total traslation error: 34.833945083870184 cm
total rotation error: 1.1298143850552094
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e35_0.102.tar
This is 36-th epoch
Iter 50/635 training loss = 0.095, time = 0.0157, time for 50 iter: 302.2688
Iter 100/635 training loss = 0.090, time = 0.0158, time for 50 iter: 354.9018
Iter 150/635 training loss = 0.090, time = 0.0156, time for 50 iter: 365.3919
Iter 200/635 training loss = 0.093, time = 0.0160, time for 50 iter: 352.2197
Iter 250/635 training loss = 0.091, time = 0.0160, time for 50 iter: 243.2697
Iter 300/635 training loss = 0.093, time = 0.0156, time for 50 iter: 384.3158
Iter 350/635 training loss = 0.095, time = 0.0156, time for 50 iter: 333.6338
Iter 400/635 training loss = 0.097, time = 0.0158, time for 50 iter: 250.4777
Iter 450/635 training loss = 0.098, time = 0.0161, time for 50 iter: 428.9083
Iter 500/635 training loss = 0.092, time = 0.0157, time for 50 iter: 420.8922
Iter 550/635 training loss = 0.088, time = 0.0157, time for 50 iter: 293.8765
Iter 600/635 training loss = 0.088, time = 0.0156, time for 50 iter: 324.6511
------------------------------------
epoch 36 total training loss = 0.092
Total epoch time = 4343.04
------------------------------------
Iter 50 test loss = 0.099 , time = 0.01
Iter 100 test loss = 0.085 , time = 0.01
Iter 150 test loss = 0.110 , time = 0.01
Iter 200 test loss = 0.096 , time = 0.01
Iter 250 test loss = 0.122 , time = 0.01
------------------------------------
total test loss = 0.102
total traslation error: 34.86163520266951 cm
total rotation error: 1.1055735069023724
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e36_0.102.tar
This is 37-th epoch
Iter 50/635 training loss = 0.091, time = 0.0160, time for 50 iter: 313.9613
Iter 100/635 training loss = 0.091, time = 0.0156, time for 50 iter: 362.1985
Iter 150/635 training loss = 0.093, time = 0.0164, time for 50 iter: 252.0145
Iter 200/635 training loss = 0.090, time = 0.0162, time for 50 iter: 344.6169
Iter 250/635 training loss = 0.088, time = 0.0156, time for 50 iter: 392.9880
Iter 300/635 training loss = 0.091, time = 0.0155, time for 50 iter: 282.6058
Iter 350/635 training loss = 0.095, time = 0.0155, time for 50 iter: 404.7854
Iter 400/635 training loss = 0.090, time = 0.0155, time for 50 iter: 363.0813
Iter 450/635 training loss = 0.090, time = 0.0158, time for 50 iter: 250.7735
Iter 500/635 training loss = 0.091, time = 0.0156, time for 50 iter: 396.4887
Iter 550/635 training loss = 0.089, time = 0.0158, time for 50 iter: 447.7478
Iter 600/635 training loss = 0.091, time = 0.0159, time for 50 iter: 311.6609
------------------------------------
epoch 37 total training loss = 0.090
Total epoch time = 4290.59
------------------------------------
Iter 50 test loss = 0.094 , time = 0.01
Iter 100 test loss = 0.086 , time = 0.01
Iter 150 test loss = 0.103 , time = 0.01
Iter 200 test loss = 0.094 , time = 0.01
Iter 250 test loss = 0.110 , time = 0.01
------------------------------------
total test loss = 0.097
total traslation error: 33.73840104543908 cm
total rotation error: 1.1222287803671744
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e37_0.097.tar
This is 38-th epoch
Iter 50/635 training loss = 0.094, time = 0.0168, time for 50 iter: 280.6965
Iter 100/635 training loss = 0.089, time = 0.0159, time for 50 iter: 368.9463
Iter 150/635 training loss = 0.086, time = 0.0155, time for 50 iter: 375.5251
Iter 200/635 training loss = 0.082, time = 0.0159, time for 50 iter: 337.3690
Iter 250/635 training loss = 0.090, time = 0.0167, time for 50 iter: 297.7620
Iter 300/635 training loss = 0.090, time = 0.0157, time for 50 iter: 354.6126
Iter 350/635 training loss = 0.085, time = 0.0158, time for 50 iter: 254.4402
Iter 400/635 training loss = 0.094, time = 0.0159, time for 50 iter: 385.6320
Iter 450/635 training loss = 0.082, time = 0.0163, time for 50 iter: 376.8170
Iter 500/635 training loss = 0.090, time = 0.0162, time for 50 iter: 283.2011
Iter 550/635 training loss = 0.094, time = 0.0162, time for 50 iter: 410.2042
Iter 600/635 training loss = 0.088, time = 0.0158, time for 50 iter: 422.5640
------------------------------------
epoch 38 total training loss = 0.089
Total epoch time = 4317.24
------------------------------------
Iter 50 test loss = 0.106 , time = 0.01
Iter 100 test loss = 0.097 , time = 0.01
Iter 150 test loss = 0.107 , time = 0.01
Iter 200 test loss = 0.096 , time = 0.01
Iter 250 test loss = 0.112 , time = 0.01
------------------------------------
total test loss = 0.103
total traslation error: 35.495258088522455 cm
total rotation error: 1.116427358565575
------------------------------------
This is 39-th epoch
Iter 50/635 training loss = 0.087, time = 0.0162, time for 50 iter: 293.8064
Iter 100/635 training loss = 0.090, time = 0.0156, time for 50 iter: 295.8661
Iter 150/635 training loss = 0.083, time = 0.0159, time for 50 iter: 335.4167
Iter 200/635 training loss = 0.092, time = 0.0154, time for 50 iter: 382.5232
Iter 250/635 training loss = 0.089, time = 0.0156, time for 50 iter: 381.7105
Iter 300/635 training loss = 0.089, time = 0.0153, time for 50 iter: 302.3518
Iter 350/635 training loss = 0.086, time = 0.0159, time for 50 iter: 305.8717
Iter 400/635 training loss = 0.090, time = 0.0160, time for 50 iter: 437.9833
Iter 450/635 training loss = 0.088, time = 0.0159, time for 50 iter: 323.9176
Iter 500/635 training loss = 0.092, time = 0.0160, time for 50 iter: 305.1405
Iter 550/635 training loss = 0.083, time = 0.0155, time for 50 iter: 434.1122
Iter 600/635 training loss = 0.093, time = 0.0157, time for 50 iter: 354.9750
------------------------------------
epoch 39 total training loss = 0.088
Total epoch time = 4374.67
------------------------------------
Iter 50 test loss = 0.095 , time = 0.01
Iter 100 test loss = 0.087 , time = 0.01
Iter 150 test loss = 0.107 , time = 0.01
Iter 200 test loss = 0.100 , time = 0.01
Iter 250 test loss = 0.118 , time = 0.01
------------------------------------
total test loss = 0.101
total traslation error: 34.80008548819738 cm
total rotation error: 1.1162548592321098
------------------------------------
This is 40-th epoch
Iter 50/635 training loss = 0.091, time = 0.0157, time for 50 iter: 301.8039
Iter 100/635 training loss = 0.087, time = 0.0157, time for 50 iter: 364.8996
Iter 150/635 training loss = 0.088, time = 0.0160, time for 50 iter: 327.9052
Iter 200/635 training loss = 0.086, time = 0.0157, time for 50 iter: 280.1744
Iter 250/635 training loss = 0.092, time = 0.0157, time for 50 iter: 388.6529
Iter 300/635 training loss = 0.088, time = 0.0158, time for 50 iter: 369.3710
Iter 350/635 training loss = 0.086, time = 0.0156, time for 50 iter: 275.2207
Iter 400/635 training loss = 0.088, time = 0.0156, time for 50 iter: 355.6422
Iter 450/635 training loss = 0.088, time = 0.0155, time for 50 iter: 415.7575
Iter 500/635 training loss = 0.086, time = 0.0162, time for 50 iter: 262.3382
Iter 550/635 training loss = 0.080, time = 0.0155, time for 50 iter: 357.6414
Iter 600/635 training loss = 0.089, time = 0.0158, time for 50 iter: 431.9739
------------------------------------
epoch 40 total training loss = 0.088
Total epoch time = 4414.14
------------------------------------
Iter 50 test loss = 0.104 , time = 0.01
Iter 100 test loss = 0.094 , time = 0.01
Iter 150 test loss = 0.113 , time = 0.01
Iter 200 test loss = 0.103 , time = 0.01
Iter 250 test loss = 0.117 , time = 0.01
------------------------------------
total test loss = 0.106
total traslation error: 36.29687976480134 cm
total rotation error: 1.1380448876905747
------------------------------------
This is 41-th epoch
Iter 50/635 training loss = 0.088, time = 0.0157, time for 50 iter: 299.6797
Iter 100/635 training loss = 0.083, time = 0.0159, time for 50 iter: 243.0935
Iter 150/635 training loss = 0.083, time = 0.0159, time for 50 iter: 337.3785
Iter 200/635 training loss = 0.080, time = 0.0154, time for 50 iter: 380.8443
Iter 250/635 training loss = 0.082, time = 0.0158, time for 50 iter: 389.2261
Iter 300/635 training loss = 0.081, time = 0.0156, time for 50 iter: 364.4725
Iter 350/635 training loss = 0.089, time = 0.0156, time for 50 iter: 256.2620
Iter 400/635 training loss = 0.080, time = 0.0158, time for 50 iter: 387.9687
Iter 450/635 training loss = 0.089, time = 0.0160, time for 50 iter: 429.9312
Iter 500/635 training loss = 0.084, time = 0.0156, time for 50 iter: 268.6815
Iter 550/635 training loss = 0.094, time = 0.0157, time for 50 iter: 323.1888
Iter 600/635 training loss = 0.091, time = 0.0157, time for 50 iter: 422.8875
------------------------------------
epoch 41 total training loss = 0.085
Total epoch time = 4271.20
------------------------------------
Iter 50 test loss = 0.091 , time = 0.01
Iter 100 test loss = 0.083 , time = 0.01
Iter 150 test loss = 0.100 , time = 0.01
Iter 200 test loss = 0.098 , time = 0.01
Iter 250 test loss = 0.119 , time = 0.01
------------------------------------
total test loss = 0.098
total traslation error: 33.84908606338753 cm
total rotation error: 1.0766765934285338
------------------------------------
This is 42-th epoch
Iter 50/635 training loss = 0.086, time = 0.0153, time for 50 iter: 339.8680
Iter 100/635 training loss = 0.082, time = 0.0159, time for 50 iter: 350.0544
Iter 150/635 training loss = 0.081, time = 0.0160, time for 50 iter: 279.8003
Iter 200/635 training loss = 0.083, time = 0.0155, time for 50 iter: 299.6843
Iter 250/635 training loss = 0.081, time = 0.0157, time for 50 iter: 376.0478
Iter 300/635 training loss = 0.082, time = 0.0153, time for 50 iter: 396.5346
Iter 350/635 training loss = 0.086, time = 0.0159, time for 50 iter: 279.8348
Iter 400/635 training loss = 0.084, time = 0.0157, time for 50 iter: 337.9042
Iter 450/635 training loss = 0.083, time = 0.0157, time for 50 iter: 412.8315
Iter 500/635 training loss = 0.078, time = 0.0159, time for 50 iter: 246.4147
Iter 550/635 training loss = 0.081, time = 0.0156, time for 50 iter: 373.8188
Iter 600/635 training loss = 0.084, time = 0.0154, time for 50 iter: 376.2472
------------------------------------
epoch 42 total training loss = 0.083
Total epoch time = 4349.03
------------------------------------
Iter 50 test loss = 0.091 , time = 0.01
