INFO - CMRNet - Running command 'main'
INFO - CMRNet - Started run with ID "2"
simple
Test Sequence:  00
TEST SET: Using this file: /storage/odometry/dataset/sequences/test_RT_seq00_2.00_1.00.csv
11423
635
253
Loading weights from ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e7_0.241.tar
11423
Number of model parameters: 37116287
This is 7-th epoch
/home/cmr_user/.local/lib/python3.6/site-packages/torch/optim/lr_scheduler.py:417: UserWarning: To get the last learning rate computed by the scheduler, please use `get_last_lr()`.
  "please use `get_last_lr()`.", UserWarning)
Iter 50/635 training loss = 0.246, time = 0.0160, time for 50 iter: 232.0405
Iter 100/635 training loss = 0.248, time = 0.0157, time for 50 iter: 229.1766
Iter 150/635 training loss = 0.237, time = 0.0161, time for 50 iter: 231.5156
Iter 200/635 training loss = 0.235, time = 0.0162, time for 50 iter: 230.0753
Iter 250/635 training loss = 0.245, time = 0.0156, time for 50 iter: 228.1557
Iter 300/635 training loss = 0.241, time = 0.0161, time for 50 iter: 230.5285
Iter 350/635 training loss = 0.238, time = 0.0160, time for 50 iter: 228.7635
Iter 400/635 training loss = 0.232, time = 0.0158, time for 50 iter: 246.5181
Iter 450/635 training loss = 0.234, time = 0.0158, time for 50 iter: 253.0599
Iter 500/635 training loss = 0.219, time = 0.0160, time for 50 iter: 303.9291
Iter 550/635 training loss = 0.233, time = 0.0154, time for 50 iter: 274.4329
Iter 600/635 training loss = 0.234, time = 0.0157, time for 50 iter: 275.3572
------------------------------------
epoch 7 total training loss = 0.236
Total epoch time = 3192.64
------------------------------------
Iter 50 test loss = 0.232 , time = 0.01
Iter 100 test loss = 0.209 , time = 0.01
Iter 150 test loss = 0.217 , time = 0.01
Iter 200 test loss = 0.247 , time = 0.01
Iter 250 test loss = 0.243 , time = 0.01
------------------------------------
total test loss = 0.229
total traslation error: 57.49894707971038 cm
total rotation error: 1.582454221045438
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e7_0.229.tar
This is 8-th epoch
Iter 50/635 training loss = 0.230, time = 0.0167, time for 50 iter: 280.6712
Iter 100/635 training loss = 0.240, time = 0.0169, time for 50 iter: 323.6440
Iter 150/635 training loss = 0.235, time = 0.0161, time for 50 iter: 297.4661
Iter 200/635 training loss = 0.235, time = 0.0162, time for 50 iter: 237.3329
Iter 250/635 training loss = 0.236, time = 0.0163, time for 50 iter: 294.4325
Iter 300/635 training loss = 0.226, time = 0.0164, time for 50 iter: 335.1774
Iter 350/635 training loss = 0.213, time = 0.0160, time for 50 iter: 317.1906
Iter 400/635 training loss = 0.223, time = 0.0165, time for 50 iter: 231.3734
Iter 450/635 training loss = 0.229, time = 0.0157, time for 50 iter: 327.2037
Iter 500/635 training loss = 0.219, time = 0.0165, time for 50 iter: 274.5435
Iter 550/635 training loss = 0.227, time = 0.0161, time for 50 iter: 297.5325
Iter 600/635 training loss = 0.211, time = 0.0161, time for 50 iter: 384.4623
------------------------------------
epoch 8 total training loss = 0.227
Total epoch time = 3824.36
------------------------------------
Iter 50 test loss = 0.208 , time = 0.01
Iter 100 test loss = 0.184 , time = 0.01
Iter 150 test loss = 0.207 , time = 0.01
Iter 200 test loss = 0.232 , time = 0.01
Iter 250 test loss = 0.227 , time = 0.01
------------------------------------
total test loss = 0.210
total traslation error: 54.397421646370184 cm
total rotation error: 1.533754992658308
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e8_0.210.tar
This is 9-th epoch
Iter 50/635 training loss = 0.224, time = 0.0162, time for 50 iter: 300.7936
Iter 100/635 training loss = 0.207, time = 0.0158, time for 50 iter: 360.7060
Iter 150/635 training loss = 0.218, time = 0.0160, time for 50 iter: 348.3174
Iter 200/635 training loss = 0.209, time = 0.0158, time for 50 iter: 232.2972
Iter 250/635 training loss = 0.211, time = 0.0160, time for 50 iter: 342.7383
Iter 300/635 training loss = 0.213, time = 0.0159, time for 50 iter: 380.3533
Iter 350/635 training loss = 0.227, time = 0.0164, time for 50 iter: 236.9625
Iter 400/635 training loss = 0.207, time = 0.0162, time for 50 iter: 319.5959
Iter 450/635 training loss = 0.201, time = 0.0161, time for 50 iter: 409.5242
Iter 500/635 training loss = 0.210, time = 0.0161, time for 50 iter: 294.3508
Iter 550/635 training loss = 0.216, time = 0.0159, time for 50 iter: 306.6533
Iter 600/635 training loss = 0.206, time = 0.0159, time for 50 iter: 418.4338
------------------------------------
epoch 9 total training loss = 0.211
Total epoch time = 4176.44
------------------------------------
Iter 50 test loss = 0.209 , time = 0.01
Iter 100 test loss = 0.199 , time = 0.01
Iter 150 test loss = 0.222 , time = 0.01
Iter 200 test loss = 0.234 , time = 0.01
Iter 250 test loss = 0.237 , time = 0.01
------------------------------------
total test loss = 0.219
total traslation error: 56.270857823784986 cm
total rotation error: 1.4779260852110594
------------------------------------
This is 10-th epoch
Iter 50/635 training loss = 0.224, time = 0.0168, time for 50 iter: 328.5642
Iter 100/635 training loss = 0.207, time = 0.0159, time for 50 iter: 306.6556
Iter 150/635 training loss = 0.212, time = 0.0160, time for 50 iter: 264.0255
Iter 200/635 training loss = 0.201, time = 0.0159, time for 50 iter: 364.3023
Iter 250/635 training loss = 0.203, time = 0.0162, time for 50 iter: 389.7935
Iter 300/635 training loss = 0.195, time = 0.0160, time for 50 iter: 280.1967
Iter 350/635 training loss = 0.206, time = 0.0158, time for 50 iter: 306.1458
Iter 400/635 training loss = 0.205, time = 0.0166, time for 50 iter: 406.1370
Iter 450/635 training loss = 0.210, time = 0.0158, time for 50 iter: 326.9337
Iter 500/635 training loss = 0.210, time = 0.0160, time for 50 iter: 238.4695
Iter 550/635 training loss = 0.199, time = 0.0158, time for 50 iter: 379.2762
Iter 600/635 training loss = 0.213, time = 0.0163, time for 50 iter: 383.7234
------------------------------------
epoch 10 total training loss = 0.206
Total epoch time = 4143.14
------------------------------------
Iter 50 test loss = 0.200 , time = 0.01
Iter 100 test loss = 0.172 , time = 0.01
Iter 150 test loss = 0.197 , time = 0.01
Iter 200 test loss = 0.221 , time = 0.01
Iter 250 test loss = 0.228 , time = 0.01
------------------------------------
total test loss = 0.203
total traslation error: 52.57952660428509 cm
total rotation error: 1.526179417616679
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e10_0.203.tar
This is 11-th epoch
Iter 50/635 training loss = 0.195, time = 0.0159, time for 50 iter: 253.1193
Iter 100/635 training loss = 0.193, time = 0.0162, time for 50 iter: 370.8015
Iter 150/635 training loss = 0.197, time = 0.0162, time for 50 iter: 324.5732
Iter 200/635 training loss = 0.199, time = 0.0162, time for 50 iter: 290.1405
Iter 250/635 training loss = 0.193, time = 0.0162, time for 50 iter: 244.8133
Iter 300/635 training loss = 0.201, time = 0.0165, time for 50 iter: 390.8873
Iter 350/635 training loss = 0.186, time = 0.0159, time for 50 iter: 408.5378
Iter 400/635 training loss = 0.193, time = 0.0160, time for 50 iter: 250.0165
Iter 450/635 training loss = 0.195, time = 0.0165, time for 50 iter: 309.8488
Iter 500/635 training loss = 0.211, time = 0.0157, time for 50 iter: 273.2359
Iter 550/635 training loss = 0.188, time = 0.0157, time for 50 iter: 413.4318
Iter 600/635 training loss = 0.196, time = 0.0160, time for 50 iter: 419.7529
------------------------------------
epoch 11 total training loss = 0.195
Total epoch time = 4208.40
------------------------------------
Iter 50 test loss = 0.174 , time = 0.01
Iter 100 test loss = 0.153 , time = 0.01
Iter 150 test loss = 0.183 , time = 0.01
Iter 200 test loss = 0.188 , time = 0.01
Iter 250 test loss = 0.196 , time = 0.01
------------------------------------
total test loss = 0.178
total traslation error: 49.17742459234806 cm
total rotation error: 1.4524111884250905
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e11_0.178.tar
This is 12-th epoch
Iter 50/635 training loss = 0.200, time = 0.0160, time for 50 iter: 283.5093
Iter 100/635 training loss = 0.192, time = 0.0160, time for 50 iter: 279.9797
Iter 150/635 training loss = 0.188, time = 0.0157, time for 50 iter: 370.1726
Iter 200/635 training loss = 0.185, time = 0.0159, time for 50 iter: 398.0400
Iter 250/635 training loss = 0.186, time = 0.0154, time for 50 iter: 244.0128
Iter 300/635 training loss = 0.191, time = 0.0154, time for 50 iter: 396.2565
Iter 350/635 training loss = 0.190, time = 0.0164, time for 50 iter: 259.5568
Iter 400/635 training loss = 0.189, time = 0.0156, time for 50 iter: 300.0070
Iter 450/635 training loss = 0.192, time = 0.0161, time for 50 iter: 427.9320
Iter 500/635 training loss = 0.179, time = 0.0155, time for 50 iter: 444.1096
Iter 550/635 training loss = 0.182, time = 0.0158, time for 50 iter: 309.8967
Iter 600/635 training loss = 0.180, time = 0.0159, time for 50 iter: 249.7629
------------------------------------
epoch 12 total training loss = 0.188
Total epoch time = 4235.92
------------------------------------
Iter 50 test loss = 0.173 , time = 0.01
Iter 100 test loss = 0.144 , time = 0.01
Iter 150 test loss = 0.170 , time = 0.01
Iter 200 test loss = 0.176 , time = 0.01
Iter 250 test loss = 0.202 , time = 0.01
------------------------------------
total test loss = 0.172
total traslation error: 48.27868321025406 cm
total rotation error: 1.4689170109283238
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e12_0.172.tar
This is 13-th epoch
Iter 50/635 training loss = 0.189, time = 0.0158, time for 50 iter: 335.9238
Iter 100/635 training loss = 0.184, time = 0.0162, time for 50 iter: 267.2284
Iter 150/635 training loss = 0.184, time = 0.0158, time for 50 iter: 368.9742
Iter 200/635 training loss = 0.185, time = 0.0156, time for 50 iter: 309.5623
Iter 250/635 training loss = 0.181, time = 0.0162, time for 50 iter: 377.0830
Iter 300/635 training loss = 0.187, time = 0.0155, time for 50 iter: 342.3915
Iter 350/635 training loss = 0.184, time = 0.0164, time for 50 iter: 286.7559
Iter 400/635 training loss = 0.183, time = 0.0163, time for 50 iter: 408.7172
Iter 450/635 training loss = 0.191, time = 0.0160, time for 50 iter: 317.3972
Iter 500/635 training loss = 0.173, time = 0.0157, time for 50 iter: 303.0430
Iter 550/635 training loss = 0.170, time = 0.0156, time for 50 iter: 281.2761
Iter 600/635 training loss = 0.169, time = 0.0166, time for 50 iter: 287.2829
------------------------------------
epoch 13 total training loss = 0.182
Total epoch time = 4185.32
------------------------------------
Iter 50 test loss = 0.168 , time = 0.01
Iter 100 test loss = 0.141 , time = 0.01
Iter 150 test loss = 0.162 , time = 0.01
Iter 200 test loss = 0.177 , time = 0.01
Iter 250 test loss = 0.191 , time = 0.01
------------------------------------
total test loss = 0.167
total traslation error: 47.36503825537357 cm
total rotation error: 1.404026549599082
------------------------------------
Model saved as ./checkpoints_test_2/kitti/00/checkpoint_r2.00_t1.00_e13_0.167.tar
This is 14-th epoch
Iter 50/635 training loss = 0.185, time = 0.0157, time for 50 iter: 322.2038
Iter 100/635 training loss = 0.171, time = 0.0162, time for 50 iter: 347.1079
Iter 150/635 training loss = 0.174, time = 0.0158, time for 50 iter: 331.4682
Iter 200/635 training loss = 0.179, time = 0.0156, time for 50 iter: 243.7963
Iter 250/635 training loss = 0.176, time = 0.0160, time for 50 iter: 357.6332
Iter 300/635 training loss = 0.166, time = 0.0156, time for 50 iter: 267.0374
Iter 350/635 training loss = 0.176, time = 0.0155, time for 50 iter: 386.5988
Iter 400/635 training loss = 0.176, time = 0.0158, time for 50 iter: 332.4909
Iter 450/635 training loss = 0.170, time = 0.0160, time for 50 iter: 302.5596
Iter 500/635 training loss = 0.182, time = 0.0158, time for 50 iter: 385.4725
Iter 550/635 training loss = 0.162, time = 0.0158, time for 50 iter: 358.3715
Iter 600/635 training loss = 0.179, time = 0.0163, time for 50 iter: 238.6565
------------------------------------
epoch 14 total training loss = 0.174
Total epoch time = 4120.57
------------------------------------
Iter 50 test loss = 0.156 , time = 0.01
Iter 100 test loss = 0.141 , time = 0.01
