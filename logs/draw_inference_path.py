import argparse
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd

matplotlib.use('TkAgg')

def parse_args():
    parser = argparse.ArgumentParser(description='Visualize 2d inference path')
    parser.add_argument('--logfile', required=True, help='Path to log file')
    return parser.parse_args()


def draw_2d_plot(gtx, gty, gpsx, gpsy, predx, predy):
    plt.figure()
    plt.plot(gtx, gty, label = 'gt')
    plt.plot(gpsx, gpsy, label = 'gps')
    plt.plot(predx, predy, label = 'pred')
    plt.axis('equal')
    plt.grid()
    plt.legend()
    plt.show()


if __name__ == '__main__':

    args = parse_args()
    poses = pd.read_csv(args.logfile)

    draw_2d_plot(poses.loc[:, 'gt_x'], poses.loc[:, 'gt_y'],
                 poses.loc[:, 'gps_x'], poses.loc[:, 'gps_y'],
                 poses.loc[:, 'pred_x'], poses.loc[:, 'pred_y'])






