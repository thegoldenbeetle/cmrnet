import argparse
import glob
import json
import matplotlib.pyplot as plt
import numpy as np

TRAIN_LOSS_FIELD = 'Total training loss'
VAL_LOSS_FIELD = 'Val_Loss'
VAL_R_ERROR = 'Val_r_error'
VAL_T_ERROR = 'Val_t_error'
STEPS = 'steps'
VALUES = 'values'


def parse_args():
    parser = argparse.ArgumentParser(description='Get training and validation plots.')
    parser.add_argument('--logdir', required=True, help='Path to logs')
    return parser.parse_args()


def draw_plots(train_steps, train_loss, val_steps, val_loss, val_r_error, val_t_error):
    plt.rcParams["figure.figsize"] = [18, 4]
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    fig.suptitle('Plots')
    ax1.set_title('Loss')
    ax1.plot(train_steps, train_loss, label='train loss')
    ax1.plot(val_steps, val_loss, label='val loss')
    ax1.grid()
    ax1.legend()
    ax2.set_title('Rotation error')
    ax2.plot(val_steps, val_r_error)
    ax2.grid()
    ax3.set_title('Translation error')
    ax3.plot(val_steps, val_t_error)
    ax3.grid()
    plt.show()


if __name__ == "__main__":
    
    args = parse_args()
    log_files = sorted(glob.glob(f'{args.logdir}/*/metrics.json'))
    
    train_steps = []
    train_loss = []
    val_steps = []
    val_loss = []
    val_r_error = []
    val_t_error = []
    
    for log_file in log_files:
        with open(log_file) as file:
            data = json.load(file)
            train_steps.extend(data[TRAIN_LOSS_FIELD][STEPS])
            train_loss.extend(data[TRAIN_LOSS_FIELD][VALUES])
            val_steps.extend(data[VAL_LOSS_FIELD][STEPS])
            val_loss.extend(data[VAL_LOSS_FIELD][VALUES])
            val_r_error.extend(data[VAL_R_ERROR][VALUES])
            val_t_error.extend(data[VAL_T_ERROR][VALUES])

    draw_plots(train_steps, train_loss, val_steps, val_loss, val_r_error, val_t_error)




        
